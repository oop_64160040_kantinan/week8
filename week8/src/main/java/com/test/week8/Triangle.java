package com.test.week8;

public class Triangle {
    private int a;
    private int b;
    private int c;
    private int s;
    private int areaT;
    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public int printTrianglePerimeter() {
        s = (a+b+c) / 2;
        System.out.println("Perimeter of triangle = "+ s);
        return s;
    }
    public int printTriangleArea() {
        s = (a+b+c) / 2;
        areaT = (int) Math.sqrt(s * (s-a) * (s-b) * (s-c));
        System.out.println("Area of triangle = "+ areaT);
        return areaT;
    }
}
