package com.test.week8;

public class Circle {
    private double radius;
    private Double areaC;
    private double perC;
    public Circle(double d) {
        this.radius = d;
    }
    public Double printCircleArea() {
        areaC = (Double) (3.14 * radius * radius);
        System.out.println("Area of Circle = "+ areaC);
        return areaC;
    }
    public Double printCirclePerimeter() {
        perC = 2 * 3.14 * radius;
        System.out.println("Perimeter of Circle = "+ perC);
        return perC;
    }
}
