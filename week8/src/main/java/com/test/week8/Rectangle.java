package com.test.week8;

public class Rectangle {
    private int height;
    private int width;
    private int areaR;
    private int perR;
    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }
    public int printRectangleArea1() {
        areaR = height * width;
        System.out.println("Area of rectangle1 = "+ areaR);
        return areaR;
    }
    public int printRectangleArea2() {
        areaR = height * width;
        System.out.println("Area of rectangle2 = "+ areaR);
        return areaR;
    }
    public int printRectanglePerimeter1() {
        perR = (height * 2) + (width * 2);
        System.out.println("Perimeter of rectangle1 = "+perR);
        return perR;
    }
    public int printRectanglePerimeter2() {
        perR = (height * 2) + (width * 2);
        System.out.println("Perimeter of rectangle2 = "+perR);
        return perR;
    }
}

