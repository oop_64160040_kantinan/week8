package com.test.week8;

public class TestShapeApp {
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(10,5);
        rec1.printRectangleArea1();

        Rectangle rec2 = new Rectangle(5,3);
        rec2.printRectangleArea2();

        Circle cir1 = new Circle(5);
        cir1.printCircleArea();

        Circle cir2 = new Circle(3.2);
        cir2.printCircleArea();

        Triangle tri1 = new Triangle(5, 5, 6);
        tri1.printTriangleArea();

        rec1.printRectanglePerimeter1();
        rec2.printRectanglePerimeter2();

        cir1.printCirclePerimeter();
        cir2.printCirclePerimeter();

        Triangle per1 = new Triangle(5, 5, 6);
        per1.printTrianglePerimeter();


    }
    
}
